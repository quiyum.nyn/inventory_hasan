<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 7/4/2017
 * Time: 9:44 PM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Registration_info extends Database
{
    public $id;
    public $name;
    public $contact;
    public $email;
    public $password;
    public $user_name;
    public $reg_date;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('contact', $data)) {
            $this->contact = $data['contact'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('u_name', $data)) {
            $this->user_name = $data['u_name'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = md5($data['password']);
        }



        return $this;

    }

    public function store(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->reg_date=$date;
        $query= "INSERT INTO `registration_info`(name,email,contact,user_name,password,reg_date) VALUES (?,?,?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->name);
        $STH->bindParam(2,$this->email);
        $STH->bindParam(3,$this->contact);
        $STH->bindParam(4,$this->user_name);
        $STH->bindParam(5,$this->password);
        $STH->bindParam(6,$this->reg_date);

        $result = $STH->execute();
        if($result){

            Message::setMessage("Success! Registration successfully Completed! Login Yourself");
        }
        else{
            Message::setMessage("Failed! data has not be inserted!");
        }
        Utility::redirect('../views/login.php');
    }
    public function is_exist_email(){

        $query="SELECT * FROM `registration_info` WHERE `email`='$this->email'";
        $STH=$this->DBH->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function is_exist_user(){

        $query="SELECT * FROM `registration_info` WHERE `user_name`='$this->user_name'";
        $STH=$this->DBH->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function loginCheck(){
        $query = "SELECT * FROM `registration_info` WHERE `user_name`='$this->user_name' AND `password`='$this->password'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();


        $count = $STH->rowCount();
        if ($count > 0) {

            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function adminId(){
        $sql = "SELECT id FROM `registration_info` WHERE `user_name`='$this->user_name' AND `password`='$this->password'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function showUser(){
        $sql = "SELECT * FROM `registration_info` WHERE `user_name`='$this->user_name' AND `password`='$this->password'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function log_out(){
        $_SESSION['u_name']="";
        return TRUE;
    }
    public function logged_in(){
        if ((array_key_exists('u_name', $_SESSION)) && (!empty($_SESSION['u_name']))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}