<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 7/4/2017
 * Time: 9:46 PM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Product_cat extends Database
{
    public $id;
    public $cat_name;
    public $cat_desc;
    public $date;
    public $admin_id;
    public $cat_status;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('category_name', $data)) {
            $this->cat_name = $data['category_name'];
        }
        if (array_key_exists('category_desc', $data)) {
            $this->cat_desc = $data['category_desc'];
        }
        if (array_key_exists('admin_id', $data)) {
            $this->admin_id = $data['admin_id'];
        }

        return $this;

    }
    public function store(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $query= "INSERT INTO `product_cat`(admin_id,cat_name,cat_desc,date) VALUES (?,?,?,?)";

        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->admin_id);
        $STH->bindParam(2,$this->cat_name);
        $STH->bindParam(3,$this->cat_desc);
        $STH->bindParam(4,$this->date);
        
        $result = $STH->execute();
        if($result){

            Message::setMessage("Success! Product Category Added");
        }
        else{
            Message::setMessage("Failed! data has not be inserted!");
        }
    }
    public function update(){
        $query= "UPDATE product_cat SET cat_name =?,cat_desc=? WHERE id=$this->id";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->cat_name);
        $STH->bindParam(2,$this->cat_desc);

        $result=$STH->execute();
        if($result){

            Message::setMessage("Success! Data successfully updated!");
        }
        else{
            Message::setMessage("Failed! data has not be updated!");
        }
    }

    public function showCategory(){
        $sql = "SELECT * FROM product_cat WHERE cat_status='0' AND admin_id='$this->admin_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function showOneCategory(){
        $sql = "SELECT * FROM product_cat WHERE cat_status='0' AND id=$this->id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function delete(){
        $this->cat_status='1';
        $query= "UPDATE product_cat SET cat_status =? WHERE id=$this->id";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->cat_status);
       $STH->execute();

    }
    
}