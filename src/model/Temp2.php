<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 7/21/2017
 * Time: 5:18 AM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;


class Temp2 extends Database
{
    public $id;
    public $vendor_name;
    public $prod_id;
    public $quantity;
    public $price;
    public $total_price;
    public $admin_id;
    public $contact;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('vendor_name', $data)) {
            $this->vendor_name = $data['vendor_name'];
        }
        if (array_key_exists('vendor_contact', $data)) {
            $this->contact = $data['vendor_contact'];
        }
        if (array_key_exists('product_id', $data)) {
            $this->prod_id = $data['product_id'];
        }
        if (array_key_exists('quantity', $data)) {
            $this->quantity = $data['quantity'];
        }
        if (array_key_exists('price', $data)) {
            $this->price = $data['price'];
        }
        if (array_key_exists('admin_id', $data)) {
            $this->admin_id = $data['admin_id'];
        }


        return $this;

    }
    public function store(){
        $this->total_price=(($this->quantity)*($this->price));
        $query= "INSERT INTO `temp2`(p_id,p_quantity,p_price,vendor_name,contact,total,admin_id) VALUES (?,?,?,?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->prod_id);
        $STH->bindParam(2,$this->quantity);
        $STH->bindParam(3,$this->price);
        $STH->bindParam(4,$this->vendor_name);
        $STH->bindParam(5,$this->contact);
        $STH->bindParam(6,$this->total_price);
        $STH->bindParam(7,$this->admin_id);


        $STH->execute();
    }
    public function update(){
        $this->total_price=(($this->quantity)*($this->price));
        $query= "UPDATE temp2 SET p_id =?,p_quantity=?,p_price=?,total=? WHERE id=$this->id";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->prod_id);
        $STH->bindParam(2,$this->quantity);
        $STH->bindParam(3,$this->price);
        $STH->bindParam(4,$this->total_price);

        $result=$STH->execute();
        if($result){

            Message::setMessage("Success! Data successfully updated!");
        }
        else{
            Message::setMessage("Failed! data has not be updated!");
        }
    }

    public function showData(){
        $sql = "SELECT temp2.id,temp2.p_id,temp2.p_quantity,temp2.p_price,temp2.total,product.prod_name,unit_lookup.unit FROM `temp2`,product,unit_lookup WHERE product.prod_id=temp2.p_id AND unit_lookup.id=product.unit_id AND temp2.admin_id='$this->admin_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showOneData(){
        $sql = "SELECT temp2.id,temp2.p_id,temp2.p_quantity,temp2.p_price,temp2.total,product.prod_name FROM `temp2`,product WHERE product.prod_id=temp2.p_id AND temp2.id=$this->id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function showVendor(){
        $sql = "SELECT vendor_name,admin_id,contact FROM temp2 WHERE admin_id='$this->admin_id' GROUP BY vendor_name";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function is_exist(){

        $query="SELECT * FROM `temp2` WHERE admin_id='$this->admin_id'";
        $STH=$this->DBH->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function delete(){
        $query = "DELETE FROM `temp2` WHERE admin_id='$this->admin_id'";
        $this->DBH->exec($query);
    }
    public function deleteOne(){
        $query = "DELETE FROM `temp2` WHERE id='$this->id'";
        $this->DBH->exec($query);
    }

    public function total_price(){
        $sql = "SELECT sum(`total`) as total_sum FROM `temp2` WHERE admin_id='$this->admin_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }

}