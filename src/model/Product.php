<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 7/4/2017
 * Time: 9:46 PM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Product extends Database
{
    public $id;
    public $prod_name;
    public $unit_id;
    public $cat_id;
    public $date;
    public $status;
    public $product_id;
    public $admin_id;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('product_name', $data)) {
            $this->prod_name = $data['product_name'];
        }
        if (array_key_exists('unit_id', $data)) {
            $this->unit_id = $data['unit_id'];
        }
        if (array_key_exists('cat_id', $data)) {
            $this->cat_id = $data['cat_id'];
        }
        if (array_key_exists('product_id', $data)) {
            $this->product_id = $data['product_id'];
        }
        if (array_key_exists('admin_id', $data)) {
            $this->admin_id = $data['admin_id'];
        }

        return $this;

    }
    public function store(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $query= "INSERT INTO `product`(admin_id,prod_name,unit_id,cat_id,date) VALUES (?,?,?,?,?)";

        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->admin_id);
        $STH->bindParam(2,$this->prod_name);
        $STH->bindParam(3,$this->unit_id);
        $STH->bindParam(4,$this->cat_id);
        $STH->bindParam(5,$this->date);

        $result = $STH->execute();
        if($result){

            Message::setMessage("Success! Product has been add");
        }
        else{
            Message::setMessage("Failed! data has not be inserted!");
        }
    }
    public function update(){
        $query= "UPDATE product SET prod_name =?,unit_id=?,cat_id=? WHERE prod_id=$this->id";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->prod_name);
        $STH->bindParam(2,$this->unit_id);
        $STH->bindParam(3,$this->cat_id);

        $result=$STH->execute();
        if($result){

            Message::setMessage("Success! Data successfully updated!");
        }
        else{
            Message::setMessage("Failed! data has not be updated!");
        }
    }
    public function showProduct(){
        $sql = "SELECT product.`prod_name`,product.prod_id,unit_lookup.unit,product_cat.cat_name FROM `product`,product_cat,unit_lookup WHERE product_cat.id=product.cat_id AND unit_lookup.id=product.unit_id AND product.status='0' AND product.admin_id='$this->admin_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function stock(){
        $sql = "SELECT inventory_final.*,unit_lookup.unit FROM `inventory_final`,product,unit_lookup WHERE product.prod_id=inventory_final.prod_id AND unit_lookup.id=product.unit_id AND inventory_final.admin_id='$this->admin_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function stockList(){
        $sql = "SELECT inventory_final.*,unit_lookup.unit FROM `inventory_final`,product,unit_lookup WHERE product.prod_id=inventory_final.prod_id AND unit_lookup.id=product.unit_id AND inventory_final.prod_id='$this->product_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function showProductWithPrice(){
        $sql = "SELECT product.`prod_name`,AVG(purchase_details.p_price) as avg_price,product.prod_id,unit_lookup.unit,product_cat.cat_name FROM `product`,product_cat,unit_lookup,purchase_details WHERE product_cat.id=product.cat_id AND unit_lookup.id=product.unit_id AND product.status='0' AND purchase_details.p_id=product.prod_id GROUP BY purchase_details.p_id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showAllProduct(){
        $sql = "SELECT product.`prod_name`,product.prod_id,unit_lookup.unit,product_cat.cat_name FROM `product`,product_cat,unit_lookup WHERE product_cat.id=product.cat_id AND unit_lookup.id=product.unit_id AND product.admin_id='$this->admin_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showOneProduct(){
        $sql = "SELECT product.`prod_name`,product.prod_id,unit_lookup.unit,product_cat.cat_name,product_cat.id as cat_id,unit_lookup.id as unit_id FROM `product`,product_cat,unit_lookup WHERE product_cat.id=product.cat_id AND unit_lookup.id=product.unit_id AND product.status='0' AND product.prod_id='$this->id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function is_exist(){

        $query="SELECT * FROM `product` WHERE `prod_name`='$this->prod_name' AND unit_id='$this->unit_id' AND cat_id='$this->cat_id' AND admin_id='$this->admin_id'";
        $STH=$this->DBH->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function delete(){
        $query = "DELETE FROM `product` WHERE prod_id=$this->id";
        $this->DBH->exec($query);
    }

    public function deleteProduct(){
        $this->status='1';
        $query= "UPDATE product SET status =? WHERE prod_id=$this->id";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->status);
       

        $result=$STH->execute();
        if($result){

            Message::setMessage("Success! Data successfully updated!");
        }
        else{
            Message::setMessage("Failed! data has not be updated!");
        }
    }

}