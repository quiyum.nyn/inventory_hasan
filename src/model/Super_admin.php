<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 7/4/2017
 * Time: 9:47 PM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Super_admin extends Database
{
    public $id;
    public $password;
    public $user_name;
    public $picture;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('u_name', $data)) {
            $this->user_name = $data['u_name'];
        }
        if (array_key_exists('picture_name', $data)) {
            $this->picture = $data['picture_name'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = md5($data['password']);
        }



        return $this;

    }

    public function loginCheck(){
        $query = "SELECT * FROM `superadmin` WHERE `username`='$this->user_name' AND `password`='$this->password'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();


        $count = $STH->rowCount();
        if ($count > 0) {

            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function showUser(){
        $sql = "SELECT * FROM `superadmin` WHERE `username`='$this->user_name' AND `password`='$this->password'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function log_out(){
        $_SESSION['u_name']="";
        return TRUE;
    }
    public function logged_in(){
        if ((array_key_exists('u_name', $_SESSION)) && (!empty($_SESSION['u_name']))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function showTotalUser(){
        $sql = "SELECT COUNT(id) as id FROM `registration_info` WHERE status='0'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function showAllUser(){
        $sql = "SELECT * FROM `registration_info` WHERE status='0'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function showOneSadmin(){
        $sql = "SELECT * FROM `superadmin` WHERE `username`='$this->user_name'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function changePic(){
        $query= "UPDATE superadmin SET picture =? WHERE id=$this->id";
        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->picture);

        $STH->execute();

    }
    public function changeSadminPass(){
        $query= "UPDATE superadmin SET password =? WHERE id=$this->id";
        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->password);

        $STH->execute();

    }
}