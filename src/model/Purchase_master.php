<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 7/4/2017
 * Time: 9:46 PM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Purchase_master extends Database
{
    public $id;
    public $date;
    public $vendor_name;
    public $total_amount;
    public $paid;
    public $due;
    public $status;
    public $mrr_no;
    public $admin_id;
    public $contact;


    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('vendor_name', $data)) {
            $this->vendor_name = $data['vendor_name'];
        }
        if (array_key_exists('vendor_contact', $data)) {
            $this->contact = $data['vendor_contact'];
        }
        if (array_key_exists('total_sum', $data)) {
            $this->total_amount = $data['total_sum'];
        }
        if (array_key_exists('paid', $data)) {
            $this->paid = $data['paid'];
        }
        if (array_key_exists('due', $data)) {
            $this->due = $data['due'];
        }
        if (array_key_exists('status', $data)) {
            $this->status = $data['status'];
        }
        if (array_key_exists('mrr_no', $data)) {
            $this->mrr_no = $data['mrr_no'];
        }
        if (array_key_exists('admin_id', $data)) {
            $this->admin_id = $data['admin_id'];
        }

        return $this;

    }
    public function store(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $query= "INSERT INTO `purchase_master`(date,vendor_name,contact,total_amount,paid,due,status,admin_id) VALUES (?,?,?,?,?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->date);
        $STH->bindParam(2,$this->vendor_name);
        $STH->bindParam(3,$this->contact);
        $STH->bindParam(4,$this->total_amount);
        $STH->bindParam(5,$this->paid);
        $STH->bindParam(6,$this->due);
        $STH->bindParam(7,$this->status);
        $STH->bindParam(8,$this->admin_id);

        $STH->execute();
        
    }
    public function newPaid(){
        $query= "UPDATE purchase_master SET paid =?,due=?,status=? WHERE mrr_no=$this->mrr_no";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->paid);
        $STH->bindParam(2,$this->due);
        $STH->bindParam(3,$this->status);
        
        $result=$STH->execute();
        if($result){

            Message::setMessage("Success! Amount has been paid!");
        }
        else{
            Message::setMessage("Failed! data has not be updated!");
        }
    }
    public function showPaidList(){
        $sql = "SELECT * FROM `purchase_master` WHERE status='0' AND admin_id='$this->admin_id' ORDER BY mrr_no DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showUnpaidList(){
        $sql = "SELECT * FROM `purchase_master` WHERE status='1' AND admin_id='$this->admin_id' ORDER BY mrr_no DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showDetails(){
        $sql = "SELECT * FROM `purchase_master` WHERE mrr_no='$this->mrr_no'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }

}