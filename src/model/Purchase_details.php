<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 7/4/2017
 * Time: 9:46 PM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Purchase_details extends Database
{
    public $id;
    public $mrr_no;
    public $p_id;
    public $p_quantity;
    public $p_price;
    public $total;
    public $master_id;
    public $date;
    public $paid;
    public $new_paid;
    public $from_date;
    public $to_date;
    public $admin_id;
    public $current_date;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('product_id', $data)) {
            $this->p_id = $data['product_id'];
        }
        if (array_key_exists('p_quantity', $data)) {
            $this->p_quantity = $data['p_quantity'];
        }
        if (array_key_exists('p_price', $data)) {
            $this->p_price = $data['p_price'];
        }
        if (array_key_exists('total_price', $data)) {
            $this->total = $data['total_price'];
        }
        if (array_key_exists('mrr_no', $data)) {
            $this->master_id = $data['mrr_no'];
        }
        if (array_key_exists('paid', $data)) {
            $this->paid = $data['paid'];
        }
        if (array_key_exists('new_paid', $data)) {
            $this->new_paid = $data['new_paid'];
        }
        if (array_key_exists('from_date', $data)) {
            $this->from_date = $data['from_date'];
        }
        if (array_key_exists('to_date', $data)) {
            $this->to_date = $data['to_date'];
        }
        if (array_key_exists('admin_id', $data)) {
            $this->admin_id = $data['admin_id'];
        }
        if (array_key_exists('current_date', $data)) {
            $this->current_date = $data['current_date'];
        }
        return $this;

    }
    public function store(){
        $select_master_id="SELECT mrr_no FROM `purchase_master` order by mrr_no DESC Limit 1";
        $STH = $this->DBH->query($select_master_id);

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        $row=$STH->fetch();
        $this->mrr_no= $row['mrr_no'];

        $pr_id=explode(",",$this->p_id);
        $quan=explode(",",$this->p_quantity);
        $price=explode(",",$this->p_price);
        $total=explode(",",$this->total);
        $count=0;
        foreach($pr_id as $pr){
            $query= "INSERT INTO purchase_details (mrr_no,p_id,p_quantity,p_price,total,admin_id) VALUES (?,?,?,?,?,?)";
            $STH = $this->DBH->prepare($query);
            $STH->bindParam(1,$this->mrr_no);
            $STH->bindParam(2,$pr_id[$count]);
            $STH->bindParam(3,$quan[$count]);
            $STH->bindParam(4,$price[$count]);
            $STH->bindParam(5,$total[$count]);
            $STH->bindParam(6,$this->admin_id);
            $STH->execute();
            $count++;
        }
    }
    public function storeBill(){
        $select_master_id="SELECT mrr_no FROM `purchase_master` order by mrr_no DESC Limit 1";
        $STH = $this->DBH->query($select_master_id);

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        $row=$STH->fetch();
        $this->mrr_no= $row['mrr_no'];

        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $query= "INSERT INTO `purchase_bill`(purchase_master_id,payment,date) VALUES (?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->mrr_no);
        $STH->bindParam(2,$this->paid);
        $STH->bindParam(3,$this->date);
        $STH->execute();
    }

    public function newPaidBill(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $query= "INSERT INTO `purchase_bill`(purchase_master_id,payment,date) VALUES (?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->master_id);
        $STH->bindParam(2,$this->new_paid);
        $STH->bindParam(3,$this->date);
        $STH->execute();
    }
    public function showBillDetails(){
        $sql = "SELECT * FROM `purchase_bill` WHERE purchase_master_id=$this->master_id ORDER BY id DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showall(){
        $sql = "SELECT product.prod_name,unit_lookup.unit,purchase_master.date,purchase_details.* FROM `purchase_details`,product,purchase_master,unit_lookup WHERE product.prod_id=purchase_details.p_id AND purchase_master.mrr_no=purchase_details.mrr_no AND product.unit_id=unit_lookup.id AND purchase_master.admin_id='$this->admin_id' ORDER BY purchase_master.date DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showSelectedDate(){
        $sql = "SELECT product.prod_name,unit_lookup.unit,purchase_master.date,purchase_details.* FROM `purchase_details`,product,purchase_master,unit_lookup WHERE product.prod_id=purchase_details.p_id AND purchase_master.mrr_no=purchase_details.mrr_no AND product.unit_id=unit_lookup.id AND purchase_master.admin_id='$this->admin_id' AND DATE(purchase_master.date) BETWEEN '$this->from_date' AND '$this->to_date' ORDER BY purchase_master.date DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showSelectedDate2(){
        $sql = "SELECT product.prod_name,unit_lookup.unit,purchase_master.date,purchase_details.* FROM `purchase_details`,product,purchase_master,unit_lookup WHERE product.prod_id=purchase_details.p_id AND purchase_master.mrr_no=purchase_details.mrr_no AND product.unit_id=unit_lookup.id AND purchase_master.admin_id='$this->admin_id' AND DATE(purchase_master.date)='$this->current_date' ORDER BY purchase_master.date DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function showListDetails(){
        $sql = "SELECT product.prod_name, purchase_details.* FROM `purchase_details`,product WHERE product.prod_id=purchase_details.p_id AND mrr_no=$this->master_id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

}