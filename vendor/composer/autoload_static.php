<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite49cf6ab32eb4020b905f01e5558c6dc
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite49cf6ab32eb4020b905f01e5558c6dc::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite49cf6ab32eb4020b905f01e5558c6dc::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
