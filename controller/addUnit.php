<?php
require_once ("../vendor/autoload.php");
use App\model\Unit_lookup;
use App\Utility\Utility;
$object=new Unit_lookup();
$object->prepareData($_POST);
$object->store();
return Utility::redirect($_SERVER['HTTP_REFERER']);