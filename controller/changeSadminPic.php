<?php
require_once("../vendor/autoload.php");
use App\model\Super_admin;
use App\Utility\Utility;
use App\Message\Message;
if(isset($_FILES['picture']['name']))
{
    $picName=time().$_FILES['picture']['name'];
    $tmp_name=$_FILES['picture']['tmp_name'];

    move_uploaded_file($tmp_name,'../resources/sadmin_photos/'.$picName);
    $_POST['picture_name']=$picName;
}
$object= new Super_admin();
$object->prepareData($_POST);
$object->changePic();
Message::setMessage("Success! Picture has been changed successfully");
return Utility::redirect($_SERVER['HTTP_REFERER']);

?>