<?php
require_once ("../vendor/autoload.php");
use App\model\Manager;
use App\Utility\Utility;
use App\Message\Message;
$object=new Manager();
$object->prepareData($_POST);
$userStatus=$object->is_exist_user();
 
if($userStatus){
    Message::setMessage("This username has been already taken!");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
else{
    if($_POST['password']==$_POST['con_password']){
        
        $object->store();
        Utility::redirect('../views/managerDetails.php');
    }
    else{
        Message::setMessage("Registration Failed! Confirm password doesn't match!");
        return Utility::redirect($_SERVER['HTTP_REFERER']);
    }
}

