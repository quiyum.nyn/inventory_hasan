<?php
require_once ("../vendor/autoload.php");
use App\Utility\Utility;
use App\Message\Message;
$object=new \App\model\Temp2();
$object->prepareData($_GET);
$object->deleteOne();
Message::setMessage("Success! Product has been delete!");
Utility::redirect('../views/purchaseProduct.php');