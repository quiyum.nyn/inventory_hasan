<?php
require_once ("../vendor/autoload.php");
use App\model\Customer;
use App\model\Temp;
use App\Message\Message;
use App\Utility\Utility;
$_POST['due']=$_POST['total_sum']-$_POST['paid'];
if($_POST['due']=='0'){
    $_POST['status']=0;
}
else
{
    $_POST['status']=1;
}
if($_POST['total_sum']<$_POST['paid']){
    Message::setMessage("Failed! payment amount can't bigger than total amount");
    Utility::redirect('../views/saleProduct.php');
}
else{
    $customer=new Customer();
    $customer->prepareData($_POST);
    $customer->store();
    $object=new \App\model\Bill_master();
    $object->prepareData($_POST);
    $object->store();
    $_POST['product_id']=implode(",",$_POST['p_id']);
    $_POST['p_quantity']=implode(",",$_POST['quantity']);
    $_POST['p_price']=implode(",",$_POST['price']);
    $_POST['total_price']=implode(",",$_POST['total']);
    $detailsObj=new \App\model\Bill_details();
    $detailsObj->prepareData($_POST);
    $detailsObj->store();
    $detailsObj->storeBill();
    $temp=new Temp();
    $temp->prepareData($_POST);
    $temp->delete();
    Message::setMessage("Success! Sale has been successfully done");
    Utility::redirect('../views/saleList.php');
}

