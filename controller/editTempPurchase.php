<?php
require_once ("../vendor/autoload.php");
use App\model\Temp2;
use App\Message\Message;
use App\Utility\Utility;
$object=new Temp2();
$object->prepareData($_POST);
$object->update();
Utility::redirect('../views/purchaseProduct.php');