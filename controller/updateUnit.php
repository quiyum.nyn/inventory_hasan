<?php
require_once ("../vendor/autoload.php");
use App\model\Unit_lookup;
use App\Utility\Utility;
$object=new Unit_lookup();
$object->prepareData($_POST);
$object->update();
return Utility::redirect($_SERVER['HTTP_REFERER']);