<?php
require_once ("../vendor/autoload.php");
use App\model\Product_cat;
use App\Utility\Utility;
$object=new Product_cat();
$object->prepareData($_POST);
$object->update();
return Utility::redirect($_SERVER['HTTP_REFERER']);