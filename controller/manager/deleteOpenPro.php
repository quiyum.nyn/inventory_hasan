<?php
require_once ("../../vendor/autoload.php");
use App\model\Opening_product;
use App\Utility\Utility;
use App\Message\Message;
$object=new Opening_product();
$object->prepareData($_GET);
$object->delete();
Message::setMessage("Success! Product has been delete!");
Utility::redirect('../../views/manager/opening_product.php');