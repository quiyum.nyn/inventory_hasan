<?php
require_once ("../../vendor/autoload.php");
use App\model\Unit_lookup;
use App\Utility\Utility;
use App\Message\Message;
$object=new Unit_lookup();
$object->prepareData($_GET);
$object->delete();
Message::setMessage("Success! Unit has been delete!");
Utility::redirect('../views/manager/productCat&Unit.php');