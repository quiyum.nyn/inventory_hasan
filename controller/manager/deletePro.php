<?php
require_once ("../../vendor/autoload.php");
use App\model\Product;
use App\Utility\Utility;
use App\Message\Message;
$object=new Product();
$object->prepareData($_GET);
$object->deleteProduct();
Message::setMessage("Success! Product has been delete!");
Utility::redirect('../../views/manager/addProduct.php');