<?php
session_start();
require_once ("../vendor/autoload.php");
use App\Utility\Utility;
use App\Message\Message;
use App\model\Registration_info;
use App\model\Manager;
use App\model\Super_admin;
if($_POST['login_status']=='0'){

    $objectAdmin=new Super_admin();
    $objectAdmin->prepareData($_POST);
    $authAdmin=$objectAdmin->loginCheck();
    if($authAdmin){
        $_SESSION['role_status']=2;
        $_SESSION['u_name']=$_POST['u_name'];
        Utility::redirect('../views/super_admin/index.php');
    }
    else{
        Message::setMessage("Username and password doesn't match! Try Again");
        Utility::redirect('../views/login.php');

    }
}
else if($_POST['login_status']=='1'){

    $object=new Registration_info();
    $object->prepareData($_POST);
    $auth=$object->loginCheck();
    $adminId=$object->adminId();
    if($auth){
        $_SESSION['role_status']=0;
        $_SESSION['u_name']=$_POST['u_name'];
        $_SESSION['admin_id']=$adminId->id;
        Utility::redirect('../views/index.php');
    }
    else{
        Message::setMessage("Username and password doesn't match! Try Again");
        Utility::redirect('../views/login.php');

    }
}
else if($_POST['login_status']=='2'){
    $manager=new Manager();
    $manager->prepareData($_POST);
    $auth2=$manager->loginCheck();
    $adminId2=$manager->adminId();

    if($auth2){
        $_SESSION['role_status']=1;
        $_SESSION['u_name']=$_POST['u_name'];
        $_SESSION['admin_id']=$adminId2->admin_id;
        Utility::redirect('../views/manager/index.php');
    }
    else{
        Message::setMessage("Username and password doesn't match! Try Again");
        Utility::redirect('../views/login.php');

    }
}

