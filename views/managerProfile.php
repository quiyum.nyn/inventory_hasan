<?php
session_start();
require_once ("../vendor/autoload.php");
require_once ("templateLayout/information.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Registration_info();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
use App\model\Manager;
$object=new Manager();
$object->prepareData($_SESSION);
$object->prepareData($_GET);
$oneData=$object->showOne();

?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("templateLayout/templateCss.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <?php require_once ("templateLayout/header.php")?>
            <div class="inner-block">
                <div class="row" style="min-height: 600px">
                    <div class="col-md-6 ">
                        <div class="pro-head">
                            <h2 style="text-align: center">Manager Profile</h2>
                        </div>
                        <?php
                        if(isset($_SESSION) && !empty($_SESSION['message'])) {

                            $msg = Message::getMessage();

                            echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                        }

                        ?>
                        <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="col-md-10 col-md-offset-1">
                                        <div style="height: 200px;" class="thumbnail">
                                            <img src="../resources/manager_photos/<?php echo $oneData->picture?>" class="img-responsive img-rounded">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h4 style="font-family: 'Maiandra GD';"><p style="width: 100px;float: left">Name</p>:       <?php echo $oneData->name?></h4>
                                        <h4 style="font-family: 'Maiandra GD';"><p style="width: 100px;float: left">User Name</p>:      <?php echo $oneData->user_name?></h4>
                                        <h4 style="font-family: 'Maiandra GD';"><p style="width: 100px;float: left">Contact</p>:      <?php echo $oneData->contact?></h4>
                                        <br>
                                        <a href="managerProfile.php?id=<?php echo $oneData->id?>&&pic_change=true" class="btn btn-primary" style="width: 100%">Change Picture</a>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="pro-head">
                            <h2 style="text-align: center">Manager Profile</h2>
                        </div>
                        <div style="height:400px; border: 2px #0c5577 solid">
                            <?php
                                if(isset($_GET['pic_change']) && !empty($_GET['pic_change'])){
                                    ?>
                            <div class="login-block">
                                <form action="../controller/changeManagerPic.php" method="post" enctype="multipart/form-data">
                                    <input type="file" name="picture" required="">
                                    <input type="hidden" name="id" value="<?php echo $oneData->id?>">
                                    <input type="submit" value="Change Picture" class="btn btn-primary">
                                </form>
                                </div>
                                    <?php
                                }
                            else{
                                ?>
                                <p style="line-height: 200px;text-align: center;font-size: 20px; color: #0c5577">Inventory Management System</p>
                                <?php
                                    if($oneData->status=='1'){
                                        ?>
                                        <p style="text-align: center"><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal" >Stop all the activities of this manager.</button></p>
                                        <div class="modal fade" id="myModal" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"><?php echo $oneData->name?></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h4>Are You Sure to stop all activities of this manager?</h4>
                                                        <a href="../controller/deleteManager.php?id=<?php echo $oneData->id?>" class="btn btn-primary">Yes</a>
                                                        <a href="" data-dismiss="modal" class="btn btn-danger">No</a>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <?php
                                    }
                                else{
                                    ?>
                                    <p style="text-align: center"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2" >Re-active all the activities of this manager.</button></p>
                                    <div class="modal fade" id="myModal2" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title"><?php echo $oneData->name?></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <h4>Are You Sure to re-active all activities of this manager?</h4>
                                                    <a href="../controller/activeManager.php?id=<?php echo $oneData->id?>" class="btn btn-primary">Yes</a>
                                                    <a href="" data-dismiss="modal" class="btn btn-danger">No</a>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                                <?php
                            }
                            ?>

                        </div>
                    </div>

                </div>


            </div>
            <!--inner block end here-->
            <?php require_once ("templateLayout/footer.php");?>
        </div>
    </div>
    <!--slider menu-->
    <?php require_once ("templateLayout/navigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php require_once ("templateLayout/templateScript.php")?>
</body>
</html>




