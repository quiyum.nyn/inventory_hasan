<?php
session_start();
require_once ("../vendor/autoload.php");
require_once ("templateLayout/information.php");
use App\Message\Message;
?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("templateLayout/templateCss.php");?>
</head>
<body>
<!--inner block start here-->
<div class="signup-page-main">
    <div class="signup-main">
        <div class="signup-head">
            <h1>Sign Up</h1>
        </div>
        <div class="signup-block">
            <?php




            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                $msg = Message::getMessage();

                echo "<p class='help-block' style='color: red;text-align: center'>$msg</p>";
            }

            ?>
            <form action="../controller/registration.php" method="post">
                <input type="text" name="name" placeholder="Name" required="">
                <input type="text" name="contact" placeholder="Contact No" required="">
                <input type="text" name="email" placeholder="Email" required="">
                <input type="text" name="u_name" placeholder="User Name" required="">
                <input type="password" name="password" placeholder="Password" required="">
                <input type="password" name="c_password" class="lock" placeholder="Confirm Password">

                <div class="forgot-top-grids">
                    <div class="forgot-grid">
                        <ul>
                            <li>
                                <input type="checkbox" id="brand1" value="agree" required>
                                <label for="brand1"><span></span>I agree to the terms</label>
                            </li>
                        </ul>
                    </div>

                    <div class="clearfix"> </div>
                </div>
                <input type="submit" name="Sign In" value="Sign up">
            </form>
            <div class="sign-down">
                <h4>Already have an account? <a href="login.php"> Login here.</a></h4>
            </div>
        </div>
    </div>
</div>
<!--inner block end here-->
<!--copy rights start here-->
<?php require_once ("templateLayout/footer.php");?>
<!--COPY rights end here-->
<!--scrolling js-->
<?php require_once ("templateLayout/templateScript.php")?>
</body>
</html>




