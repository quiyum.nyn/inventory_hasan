<?php
session_start();
require_once ("../vendor/autoload.php");
require_once ("templateLayout/information.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Registration_info();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
use App\model\Product_cat;
use App\model\Product;
use App\model\Unit_lookup;
$unit=new Unit_lookup();
$object=new Product_cat();
$product=new Product();
$object->prepareData($_SESSION);
$allData=$object->showCategory();
$unit->prepareData($_SESSION);
$unitData=$unit->showUnit();
$product->prepareData($_SESSION);
$productData=$product->showProduct();

?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("templateLayout/templateCss.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <?php require_once ("templateLayout/header.php")?>
            <div class="inner-block">
                <div class="row" style="min-height: 600px">

                    <div class="col-md-6">
                        <div class="product-block">
                            <div class="pro-head">
                                <h2 style="text-align: center">Add Product</h2>
                            </div>
                            <?php
                            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                                $msg = Message::getMessage();

                                echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                            }

                            ?>
                            <div class="login-block">

                                <form action="../controller/addProduct.php" method="post">
                                    <input type="text" name="product_name" placeholder="Product Name" required="">
                                    <select name="cat_id">
                                        <option value="reject">--Select a category--</option>
                                        <?php
                                            foreach ($allData as $oneData){
                                                echo "<option value='$oneData->id'>$oneData->cat_name</option>";
                                            }
                                        ?>

                                    </select>
                                    <select name="unit_id">
                                        <option value="reject">--Select unit--</option>
                                        <?php
                                        foreach ($unitData as $oneData){
                                            echo "<option value='$oneData->id'>$oneData->unit</option>";
                                        }
                                        ?>
                                    </select>
                                    <br>
                                    <input type="hidden" name="admin_id" value="<?php echo $_SESSION['admin_id']?>">
                                    <input type="submit" value="Add Product">
                                </form>
                            </div>
                            <div class="clearfix"> </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="pro-head">
                            <h2 style="text-align: center">Product Lookup</h2>
                        </div>
                        <div class="row">


                            <table id="example" class="table table-bordered table-striped" >
                                <thead>
                                <tr>
                                    <th>Serial</th>
                                    <th>Product</th>
                                    <th>Category</th>
                                    <th>Unit</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Serial</th>
                                    <th>Product</th>
                                    <th>Category</th>
                                    <th>Unit</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                $serial= 1;
                                foreach ($productData as $oneData){

                                    ?>
                                    <tr>
                                        <td><?php echo $serial?></td>
                                        <td><?php echo $oneData->prod_name?></td>
                                        <td><?php echo $oneData->cat_name?></td>
                                        <td><?php echo $oneData->unit?></td>
                                        <td style="text-align: center"><a href='updateProduct.php?id=<?php echo $oneData->prod_id?>' class='btn btn-info'><i class='fa fa-external-link-square ' aria-hidden='true'></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $serial++;
                                }
                                ?>

                                </tbody>
                            </table>




                        </div>
                    </div>
                </div>


            </div>
            <!--inner block end here-->
            <?php require_once ("templateLayout/footer.php");?>
        </div>
    </div>
    <!--slider menu-->
    <?php require_once ("templateLayout/navigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php require_once ("templateLayout/templateScript.php")?>
</body>
</html>




