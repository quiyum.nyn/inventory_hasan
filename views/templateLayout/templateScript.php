<script src="<?php echo base_url?>resources/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="<?php echo base_url?>resources/web/js/bootstrap.js"> </script>
<script src="<?php echo base_url?>resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url?>resources/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
    $(function () {
        $("#example").DataTable();
        $("#example3").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
<script>
    var toggle = true;

    $(".sidebar-icon").click(function() {
        if (toggle)
        {
            $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
            $("#menu span").css({"position":"absolute"});
        }
        else
        {
            $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
            setTimeout(function() {
                $("#menu span").css({"position":"relative"});
            }, 400);
        }
        toggle = !toggle;
    });
</script>
<!--scrolling js-->
<script src="<?php echo base_url?>resources/web/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url?>resources/web/js/scripts.js"></script>
<!--//scrolling js-->

<!-- mother grid end here-->
