<?php
session_start();
require_once ("../vendor/autoload.php");
require_once ("templateLayout/information.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Registration_info();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
use App\model\Purchase_master;
$masterObj=new Purchase_master();
$masterObj->prepareData($_GET);
$masterData=$masterObj->showDetails();
$date = date("d/m/Y", strtotime("$masterData->date"));
$time = date("h:m A", strtotime("$masterData->date"));
use App\model\Purchase_details;
$detailsObj=new Purchase_details();
$detailsObj->prepareData($_GET);
$detailsData=$detailsObj->showListDetails();
$billDetails=$detailsObj->showBillDetails();
?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("templateLayout/templateCss.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <?php require_once ("templateLayout/header.php")?>
            <div class="inner-block">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pro-head">
                            <h2 style="text-align: center">Purchase Details</h2>
                        </div>
                        <?php
                        if(isset($_SESSION) && !empty($_SESSION['message'])) {

                            $msg = Message::getMessage();

                            echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                        }

                        ?>
                        <div class="login-block">
                            <div class="row">
                                <div class="col-md-2">
                                    MRR No
                                    <input type="text" value="<?php echo $masterData->mrr_no?>" readonly>
                                </div>
                                <div class="col-md-4">
                                    Vendor Name
                                    <input type="text" value="<?php echo $masterData->vendor_name?>" readonly>
                                </div>
                                <div class="col-md-3">
                                    Date
                                    <input type="text" value="<?php echo $date?>" readonly>
                                </div>
                                <div class="col-md-3">
                                    Time
                                    <input type="text" value="<?php echo $time?>" readonly>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    Total Amount
                                    <input type="text" value="<?php echo $masterData->total_amount?>" readonly>
                                </div>
                                <div class="col-md-2">
                                    Paid Amount
                                    <input type="text" value="<?php echo $masterData->paid?>" readonly>
                                </div>
                                <div class="col-md-2">
                                    Due
                                    <input type="text" value="<?php echo $masterData->due?>" readonly>
                                </div>
                                <?php
                                if($masterData->due>0){
                                    ?>
                                    <form action="../controller/paid.php" method="post">
                                        <div class="col-md-4">
                                            Pay Option
                                            <input type="number" placeholder="Amount" name="new_paid" required>
                                        </div>
                                        <div class="col-md-2">
                                            Payment
                                            <input type="hidden" value="<?php echo $masterData->mrr_no?>" name="mrr_no">
                                            <input type="submit" value="Submit" class="btn btn-primary">
                                        </div>
                                    </form>

                                    <?php
                                }
                                ?>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="pro-head">
                            <h2 style="text-align: center">Product List</h2>
                        </div>
                        <table id="example" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Serial</th>
                                    <th>Product Name</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Serial</th>
                                    <th>Product Name</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Total</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                $serial= 1;
                                foreach ($detailsData as $oneData){
                                    ?>
                                    <tr>
                                        <td><?php echo $serial?></td>
                                        <td><?php echo $oneData->prod_name?></td>
                                        <td><?php echo $oneData->p_quantity?></td>
                                        <td><?php echo $oneData->p_price?></td>
                                        <td><?php echo $oneData->total?></td>
                                    </tr>
                                    <?php
                                    $serial++;
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    <div class="col-md-6">
                        <div class="pro-head">
                            <h2 style="text-align: center">Payment Details</h2>
                        </div>
                        <table id="example3" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Serial</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Serial</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Amount</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <?php
                            $serial= 1;
                            foreach ($billDetails as $Data){
                                $date = date("d/m/Y", strtotime("$Data->date"));
                                $time = date("h:i A", strtotime("$Data->date"));
                                ?>
                                <tr>
                                    <td><?php echo $serial?></td>
                                    <td><?php echo $date?></td>
                                    <td><?php echo $time?></td>
                                    <td><?php echo $Data->payment?></td>
                                </tr>
                                <?php
                                $serial++;
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    </div>
            </div>
        </div>
            <!--inner block end here-->
            <?php require_once ("templateLayout/footer.php");?>
    </div>
    <!--slider menu-->
    <?php require_once ("templateLayout/navigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php require_once ("templateLayout/templateScript.php")?>
</body>
</html>




