<?php
session_start();
require_once ("../vendor/autoload.php");
require_once ("templateLayout/information.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Registration_info();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
use App\model\Product_cat;
use App\model\Product;
use App\model\Unit_lookup;
$unit=new Unit_lookup();
$object=new Product_cat();
$product=new Product();
$object->prepareData($_SESSION);
$allData=$object->showCategory();
$unit->prepareData($_SESSION);
$unitData=$unit->showUnit();
$product->prepareData($_SESSION);
$productData=$product->showProduct();
$product->prepareData($_REQUEST);
$OneProduct=$product->showOneProduct();
?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("templateLayout/templateCss.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <?php require_once ("templateLayout/header.php")?>
            <div class="inner-block">
                <div class="row">
                    <div class="col-md-6">
                        <div class="product-block">
                            <div class="pro-head">
                                <h2 style="text-align: center">Update Product</h2>
                            </div>
                            <?php
                            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                                $msg = Message::getMessage();

                                echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                            }

                            ?>
                            <div class="login-block">

                                <form action="../controller/updateProduct.php" method="post">
                                    <input type="text" name="product_name" value="<?php echo $OneProduct->prod_name?>">
                                    <input type="hidden" name="id" value="<?php echo $OneProduct->prod_id?>">
                                    <select name="cat_id">
                                        <?php
                                        foreach ($allData as $oneData){
                                            ?>
                                            <option value='<?php echo $oneData->id?>' <?php if($oneData->id==$OneProduct->cat_id){ echo "selected";}?>><?php echo $oneData->cat_name?></option>
                                        <?php
                                        }
                                        ?>

                                    </select>
                                    <select name="unit_id">
                                        <?php
                                        foreach ($unitData as $oneData){
                                            ?>
                                        <option value='<?php echo $oneData->id?>' <?php if($oneData->id==$OneProduct->unit_id){echo "selected";}?>><?php echo $oneData->unit?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                    <br>
                                    <input type="submit" value="Update Product">
                                </form>
                                <br>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal" style="width: 100%;">Delete This Product</button>
                                <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title"><?php echo $OneProduct->prod_name?></h4>
                                            </div>
                                            <div class="modal-body">
                                                <h4>Are You Sure to delete this product information?</h4>
                                                <a href="../controller/deletePro.php?id=<?php echo $OneProduct->prod_id?>" class="btn btn-primary">Yes</a>
                                                <a href="" data-dismiss="modal" class="btn btn-danger">No</a>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"> </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="pro-head">
                            <h2 style="text-align: center">Product Lookup</h2>
                        </div>
                        <div class="row">


                            <table id="example" class="table table-bordered table-striped" >
                                <thead>
                                <tr>
                                    <th>Serial</th>
                                    <th>Product</th>
                                    <th>Category</th>
                                    <th>Unit</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Serial</th>
                                    <th>Product</th>
                                    <th>Category</th>
                                    <th>Unit</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                $serial= 1;
                                foreach ($productData as $oneData){

                                    ?>
                                    <tr>
                                        <td><?php echo $serial?></td>
                                        <td><?php echo $oneData->prod_name?></td>
                                        <td><?php echo $oneData->cat_name?></td>
                                        <td><?php echo $oneData->unit?></td>
                                        <td style="text-align: center"><a href='updateProduct.php?id=<?php echo $oneData->prod_id?>'' class='btn btn-info'><i class='fa fa-external-link-square' aria-hidden='true'></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $serial++;
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
            <!--inner block end here-->
            <?php require_once ("templateLayout/footer.php");?>
        </div>
    </div>
    <!--slider menu-->
    <?php require_once ("templateLayout/navigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php require_once ("templateLayout/templateScript.php")?>
</body>
</html>




