<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/information.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==1){
    $auth= new Registration_info();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('../login.php');
}
use App\model\Product_cat;
use App\model\Unit_lookup;
$unit=new Unit_lookup();
$object=new Product_cat();
$object->prepareData($_SESSION);
$allData=$object->showCategory();
$unit->prepareData($_GET);
$unit->prepareData($_SESSION);
$unitData=$unit->showUnit();
$oneUnitData=$unit->showOneUnit();

?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/templateCss.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <?php require_once ("../templateLayout/managerHeader.php")?>
            <div class="inner-block">
                <div class="row">
                    <?php
                    if(isset($_SESSION) && !empty($_SESSION['message'])) {

                        $msg = Message::getMessage();

                        echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                    }

                    ?>

                    <div class="col-md-6">
                        <div class="product-block">
                            <div class="pro-head">
                                <h2 style="text-align: center">Update Unit Lookup</h2>
                            </div>
                            <div class="login-block">
                                <form action="../../controller/manager/addUnit.php" method="post">
                                    <input type="text" name="unit" value="<?php echo $oneUnitData->unit?>">
                                    <input type="submit" value="Update Unit">
                                </form>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal" style="width: 100%;">Delete this Unit</button>
                                <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title"><?php echo $oneUnitData->unit?></h4>
                                            </div>
                                            <div class="modal-body">
                                                <h4>Are You Sure to delete this unit?</h4>
                                                <a href="../../controller/deleteUnit.php?id=<?php echo $oneUnitData->id?>" class="btn btn-primary">Yes</a>
                                                <a href="" data-dismiss="modal" class="btn btn-danger">No</a>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div>
                            <div class="pro-head">
                                <h2 style="text-align: center">Unit Details</h2>
                            </div>

                            <table id="example3" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Serial</th>
                                    <th>Unit</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Serial</th>
                                    <th>Unit</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                $serial= 1;
                                foreach ($unitData as $oneData){

                                    ?>
                                    <tr>
                                        <td><?php echo $serial?></td>
                                        <td><?php echo $oneData->unit?></td>
                                        <td style="text-align: center"><a href='updateUnit.php?id=<?php echo $oneData->id?>' class='btn btn-info'><i class='fa fa-external-link-square ' aria-hidden='true'></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $serial++;
                                }

                                ?>
                                </tbody>
                            </table>




                        </div>
                    </div>
                </div>


            </div>
            <!--inner block end here-->
            <?php require_once ("../templateLayout/footer.php");?>
        </div>
    </div>
    <!--slider menu-->
    <?php require_once ("../templateLayout/managerNavigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php require_once ("../templateLayout/templateScript.php")?>
</body>
</html>




