<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/information.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==1){
    $auth= new Registration_info();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('../login.php');
}
use App\model\Bill_master;
$masterObj=new Bill_master();
$masterObj->prepareData($_GET);
$masterData=$masterObj->showDetails();
$date = date("d/m/Y", strtotime("$masterData->date"));
$time = date("h:m A", strtotime("$masterData->date"));
use App\model\Bill_details;
$detailsObj=new Bill_details();
$detailsObj->prepareData($_GET);
$detailsData=$detailsObj->showListDetails();
?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/templateCss.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <?php require_once ("../templateLayout/managerHeader.php")?>
            <div class="inner-block">
                <div class="row" style="min-height: 600px">
                    <?php
                    if(isset($_SESSION) && !empty($_SESSION['message'])) {

                        $msg = Message::getMessage();

                        echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                    }

                    ?>

                    <div class="col-md-8 col-md-offset-2">
                        <div class="pro-head">
                            <h2 style="text-align: center">Customer Bill</h2>
                        </div>
                        <div class="row">
                            <div class="box-header">
                                <h3 class="box-title">Print Option <button type="button" onclick="printReceipt()" class="btn btn-primary pull-right">Print</button></h3><br />
                            </div>
                            <br>
                            <div style="border: 2px #0c5577 solid;min-height: 400px" id="printArea">
                                <h3 style="text-align: center;font-family: Centaur">INVENTORY MANAGEMENT SYSTEM</h3>
                                <p style="text-align: center;font-family: Centaur">An automated software system</p>
                                <br><br>
                                <div style="border: 2px #0c5577 solid; width: 200px;margin: 0 auto"> <h4 style="text-align: center;font-family: Centaur">Customer Bill</h4></div>
                                <br>
                                <div style="border: 1px black solid; width: 80%;margin: 0 auto" >
                                    <table class="table table-striped" style="width: 100%;margin: 0 auto" >
                                        <tbody>
                                        <tr>
                                            <td style="font-family: Cambria !important;" > Bill No: </td>
                                            <td>:</td>
                                            <td style="font-family: Cambria !important;"  align="right"> <?php echo $masterData->bill_no?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Cambria !important;" > Customer Name</td>
                                            <td>:</td>
                                            <td style="font-family: Cambria !important;"  align="right"> <?php echo $masterData->name?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Cambria !important;"> Customer Contact</td>
                                            <td>:</td>
                                            <td style="font-family: Cambria !important;" align="right"> <?php echo $masterData->contact?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <br>
                                <div style="width: 80%; margin: 0 auto;min-height: 600px">
                                    <table style="width: 100%;border-collapse: collapse; margin: 0 auto" border="1px black solid">

                                        <tbody>
                                        <tr>
                                            <td style="font-family: Cambria !important;text-align: center;width: 10%"><b>Serial</b></td>
                                            <td style="font-family: Cambria !important;text-align: center;width: 40%"><b>Product</b></td>
                                            <td style="font-family: Cambria !important;text-align: center;width: 15%"><b>Quantity</b></td>
                                            <td style="font-family: Cambria !important;text-align: center;width: 15%"><b>Price</b></td>
                                            <td style="font-family: Cambria !important;text-align: center;width: 20%"><b>Total</b></td>
                                        </tr>
                                        <?php
                                        $serial= 1;
                                        foreach ($detailsData as $oneData){
                                            ?>
                                            <tr>
                                                <td style="font-family: Centaur !important;text-align: center"><?php echo $serial?></td>
                                                <td style="font-family: Centaur !important;text-align: center"><?php echo $oneData->prod_name?></td>
                                                <td style="font-family: Centaur !important;text-align: center"><?php echo $oneData->quantity?></td>
                                                <td style="font-family: Centaur !important;text-align: center"><?php echo $oneData->prod_price?></td>
                                                <td style="font-family: Centaur !important;text-align: center"><?php echo $oneData->total?></td>
                                            </tr>
                                            <?php
                                            $serial++;
                                        }
                                        ?>
                                        <tr>
                                            <td style="font-family: Centaur !important;text-align: center" colspan="4">Total: ( <?php $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);$paymentWords= $f->format($masterData->total_amount);echo ucfirst($paymentWords)?> taka only. )</td>
                                            <td style="font-family: Centaur !important;text-align: center"><?php echo $masterData->total_amount?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Centaur !important;text-align: center" colspan="4">Paid</td>
                                            <td style="font-family: Centaur !important;text-align: center"><?php echo $masterData->paid?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table style="width: 100%;margin: 0 auto">
                                        <tr>
                                            <td style="font-family: Cambria !important;width: 30%" >Pay Mode: Cash.</td>
                                        </tr>

                                        <tr>
                                            <td style="font-family: Cambria !important;width: 70%"> Paid in Word: ( <?php $g = new NumberFormatter("en", NumberFormatter::SPELLOUT);$paymentWordsPaid= $g->format($masterData->paid);echo ucfirst($paymentWordsPaid)?> taka only. )</td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 80%;margin: 0 auto">
                                    <div style="width: 30%;">
                                        <hr>
                                        <p style="font-family: Cambria !important; text-align: center">Received By</p>
                                    </div>
                                </div>
                                <div style="width: 80%;margin: 0 auto">
                                    <br> <p style="font-family: Cambria !important; text-align: center;font-size:10px">This Copy is payment slip</p>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!--inner block end here-->
            <?php require_once ("../templateLayout/footer.php");?>
        </div>
    </div>
    <!--slider menu-->
    <?php require_once ("../templateLayout/managerNavigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php require_once ("../templateLayout/templateScript.php")?>
<script>
    function printReceipt()
    {
        var receipt = window.open('','','width=1176,height=1176');
        receipt.document.open("text/html");
        receipt.document.write(document.getElementById('printArea').innerHTML);
        receipt.document.close();
        receipt.print();
    }
</script>
</body>
</html>




