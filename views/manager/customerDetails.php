<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/information.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==1){
    $auth= new Registration_info();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('../login.php');
}
use App\model\Customer;
$object=new Customer();
$object->prepareData($_SESSION);
$customerData=$object->showall();

?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/templateCss.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <?php require_once ("../templateLayout/managerHeader.php")?>
            <div class="inner-block">
                <div class="row" style="min-height: 600px">
                    <?php
                    if(isset($_SESSION) && !empty($_SESSION['message'])) {
                        $msg = Message::getMessage();
                        echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                    }

                    ?>
                    <div class="col-md-10 col-md-offset-1">
                        <div class="pro-head">
                            <h2 style="text-align: center">Customer Lookup</h2>
                        </div>
                        <div class="row">


                            <table id="example" class="table table-bordered table-striped" >
                                <thead>
                                <tr>
                                    <th>Serial</th>
                                    <th>Customer Name</th>
                                    <th>Customer Contact</th>
                                    <th>View Bills</th>
                                    <th>Sale Products</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Serial</th>
                                    <th>Customer Name</th>
                                    <th>Customer Contact</th>
                                    <th>View Bills</th>
                                    <th>Sale Products</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                $serial= 1;
                                foreach ($customerData as $oneData){

                                    ?>
                                    <tr>
                                        <td><?php echo $serial?></td>
                                        <td><?php echo $oneData->name?></td>
                                        <td><?php echo $oneData->contact?></td>
                                        <td style="text-align: center"><a href='customerBills.php?id=<?php echo $oneData->id?>' class='btn btn-info'><i class='fa fa-external-link-square ' aria-hidden='true'></i></a>
                                        </td>
                                        <td style="text-align: center"><a href='saleProduct.php?customer_name=<?php echo $oneData->name?>&customer_contact=<?php echo $oneData->contact?>' class='btn btn-info'><i class='fa fa-shopping-cart' aria-hidden='true'></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $serial++;
                                }
                                ?>

                                </tbody>
                            </table>




                        </div>
                    </div>
                </div>


            </div>
            <!--inner block end here-->
            <?php require_once ("../templateLayout/footer.php");?>
        </div>
    </div>
    <!--slider menu-->
    <?php require_once ("../templateLayout/managerNavigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php require_once ("../templateLayout/templateScript.php")?>
</body>
</html>




