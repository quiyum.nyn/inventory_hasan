<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/information.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==1){
    $auth= new Registration_info();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
use App\model\Product;
$product=new Product();
$product->prepareData($_SESSION);
$stock=$product->stock();
?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/templateCss.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <!--header start here-->
           <?php require_once ("../templateLayout/managerHeader.php")?>
            <!-- /script-for sticky-nav -->
            <!--inner block start here-->
            <div class="inner-block" style="min-height: 600px">
                <!--market updates updates-->
                <div class="chit-chat-layer1">
                    <div class="col-md-12 chit-chat-layer1-left">
                        <div class="work-progres">
                            <div class="chit-chat-heading">
                                Sale & stock statistics
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Serial</th>
                                        <th>Product</th>
                                        <th>Sale Percentage</th>
                                        <th>Stock Percentage</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=1;
                                    foreach ($stock as $oneData){
                                        $sale=intval(($oneData->cr_qty)/($oneData->dr_qty)*100);
                                        $bal=100-$sale;
                                        ?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $oneData->prod_name;?></td>
                                            <td><?php echo $sale."%";?></td>
                                            <td><?php echo $bal."%";?></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="market-updates">
                    <div class="row">
                        <?php
                        $serial= 1;
                        foreach ($stock as $oneData){
                            ?>
                            <div class="col-md-4 market-update-gd">
                                <div class="market-update-block clr-block-1">
                                    <div class="col-md-8 market-update-left">
                                        <h4><?php echo $oneData->prod_name?></h4>
                                        <h4><?php echo $oneData->bal_qty." ".$oneData->unit?></h4>

                                        <p>Purchase Quantity: <?php echo $oneData->dr_qty." ".$oneData->unit?></p>
                                        <p>Sale Quantity: <?php echo $oneData->cr_qty." ".$oneData->unit?></p>
                                    </div>
                                    <div class="col-md-4 market-update-right">
                                        <a href="stock.php"><i class="fa fa-file-text-o"> </i></a>
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                            <?php
                            $serial++;
                        }
                        ?>

                    </div>

                </div>
                <!--market updates end here-->
                <!--mainpage chit-chating-->

                <!--main page chit chating end here-->
                <!--main page chart start here-->
                <!--main page chart layer2-->


                <!--climate start here-->

                <!--climate end here-->
            </div>
            <!--inner block end here-->
            <!--copy rights start here-->
            <?php require_once ("../templateLayout/footer.php");?>
            <!--COPY rights end here-->
        </div>
    </div>
    <!--navigation-->
   <?php require_once ("../templateLayout/managerNavigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php require_once ("../templateLayout/templateScript.php")?>
</body>
</html>