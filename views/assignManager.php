<?php
session_start();
require_once ("../vendor/autoload.php");
require_once ("templateLayout/information.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Registration_info();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}



?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("templateLayout/templateCss.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <?php require_once ("templateLayout/header.php")?>
            <div class="inner-block">
                <div class="row" style="min-height: 600px">

                    <div class="col-md-6 col-md-offset-3">
                        <div class="product-block">
                            <div class="pro-head">
                                <h2 style="text-align: center">Add Manager</h2>
                            </div>
                            <?php
                            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                                $msg = Message::getMessage();

                                echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                            }

                            ?>
                            <div class="login-block">

                                <form action="../controller/addManager.php" method="post">
                                    <input type="text" name="manager_name" placeholder="Manager Name" required="">
                                    <input type="text" name="manager_contact" placeholder="Manager Contact" required="">
                                    <input type="text" name="user_name" placeholder="User Name" required="">
                                    <input type="password" name="password" placeholder="Enter Password" required="">
                                    <input type="password" name="con_password" placeholder="Confirm Password" required="">
                                    <br>
                                    <input type="hidden" name="admin_id" value="<?php echo $_SESSION['admin_id']?>">
                                    <input type="submit" value="Assign Manager">
                                </form>
                            </div>
                            <div class="clearfix"> </div>

                        </div>
                    </div>
                </div>


            </div>
            <!--inner block end here-->
            <?php require_once ("templateLayout/footer.php");?>
        </div>
    </div>
    <!--slider menu-->
    <?php require_once ("templateLayout/navigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php require_once ("templateLayout/templateScript.php")?>
</body>
</html>




