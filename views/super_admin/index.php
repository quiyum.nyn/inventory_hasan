<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/information.php");
use App\model\Registration_info;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==2){
    $auth= new Registration_info();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
use App\model\Super_admin;
$obj=new Super_admin();
$count=$obj->showTotalUser();
$allUser=$obj->showAllUser();
?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/templateCss.php");?>
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <!--header start here-->
            <?php require_once ("../templateLayout/superAdminHeader.php")?>
            <!-- /script-for sticky-nav -->
            <!--inner block start here-->
            <div class="inner-block" style="min-height: 600px">
                <!--market updates updates-->
                <div class="chit-chat-layer1">
                    <div class="col-md-12 chit-chat-layer1-left">
                        <div class="work-progres">
                            <div class="chit-chat-heading">
                                Total User (<?php echo $count->id?>)
                            </div>
                                <table class="table table-hover" id="example">
                                    <thead>
                                    <tr>
                                        <th>Serial</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>User Name</th>
                                        <th>Register status</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=1;
                                    foreach ($allUser as $oneData){
                                        $today=date("Y-m-d");
                                        $datetime1 = new DateTime("$today");

                                        $datetime2 = new DateTime("$oneData->reg_date");

                                        $difference = $datetime1->diff($datetime2);
                                        ?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $oneData->name;?></td>
                                            <td><?php echo $oneData->email;?></td>
                                            <td><?php echo $oneData->user_name;?></td>
                                            <td><?php echo $difference->y.' years ' .$difference->m. ' months '.$difference->d.' days ago' ?></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>

                                    </tbody>
                                </table>
                           
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>

            </div>
            <!--inner block end here-->
            <!--copy rights start here-->
            <?php require_once ("../templateLayout/footer.php");?>
            <!--COPY rights end here-->
        </div>
    </div>
    <!--navigation-->
    <?php require_once ("../templateLayout/superAdminNavigation.php");?>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<?php require_once ("../templateLayout/templateScript.php")?>
</body>
</html>